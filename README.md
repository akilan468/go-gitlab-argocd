# Gitlab - Golang - Argocd demo

- Make a commit, Gitlab CI/CD will create a docker image
- docker container run --name hello-go --rm -p 8000:3000 registry.gitlab.com/akilan468/go-gitlab-argocd:latest
- http://localhost:8000

### Host helm repo in Github
- [Github page for hosting helm repo](https://medium.com/@mattiaperi/create-a-public-helm-chart-repository-with-github-pages-49b180dbb417)
- create a empty repo and clone it locally
``` bash
git clone git@github.com:akilans/helm-chart.git
helm create hello-go
# add dep.yaml and svc.yaml file
# update values.yaml
helm lint hello-go
helm package hello-go/
helm repo index --url https://akilans.github.io/helm-chart .
git add . && git commit -m "hello-go chart added" && git push -u origin master
helm repo add akilanhelmrepo https://akilans.github.io/helm-chart/
helm repo list
helm search repo hello-go
# Add new charts to an existing repository
helm repo index --url https://akilans.github.io/helm-chart/ --merge index.yaml .
```
